UNAME := $(shell uname)
help:
	@echo "run	- Runs container and mounts $(CURDIR)/server/ to /var/www/"

run: build
	docker run --name=fmfcc-hv-app -P -d -v=$(CURDIR)/server/:/var/www/:rw fmfcc-hv-app
	make info
	
build:
	docker build -t="fmfcc-hv-app" .

rebuild: delete run

delete:
	docker rm -f fmfcc-hv-app

shell:
	docker exec -ti fmfcc-hv-app /bin/bash

info: 
	docker port fmfcc-hv-app
	ifeq ($(UNAME), Darwin)
		docker-machine ip
	endif

status: 
	docker ps

whatever w:
	@./start.sh
