#FMFCC - Help4Vets

The Help4Vets app provides access to a Veteran's assistance database. It allows veterans and anonymous users to submit contact information for veterans in need, either the user themselves or someone they know.

## Overview

### Terms
**`field?` means field is optional, all other fields are required**

**`field[]` means field contains 0 or more entries**

- Contact
    - Data: phone, email, address, state
    - Relationships: Veteran, User
- Veteran
     - Data: verified, notes?
     - Relationships: Request, CBAB/PeerNavigator(User types, relationship is through Request), Contact
- User
    - Data: type
    - Relationships: Contact[]?, Request[]?
- Resource
    - Data: active, notes?
    - Relationships: Contact?, Request[]?

### User types
- Administrators
    - Can monitor (and potentially modify) submitted request
    - Can manage users, contacts, resources, etc
- Peer Navigators/CBABs
    - Assigns and is assigned requests
    - Assigns resources and add notes to requests
    - Manage resources
- Submitter
    - Can self-identify as a veteran
    - Can provide own contact info

>### How to spin up dev environment.
>`docker-compose up -d`

### Alternative (bad) dev environment
the docker-compose.yml file has been edited to only spin up a database while file mounts are sorted out. configure `server/.env` with whatever database you're using.

### Either way
fastest development path is

        terminal 1 in root of project: docker-compose up
        terminal 2 in server folder: php artisan serve

> Please note that in OSX you will want to use the Docker Private Beta https://beta.docker.com/
