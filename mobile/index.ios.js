/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {
  AppRegistry,
} from 'react-native';

import Request from './src/Request';

AppRegistry.registerComponent('mobile', () => Request);
