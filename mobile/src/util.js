import _ from 'lodash'

export const __DEBUG__ = true;

export const OSM_API_URL = "https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lon}";

export const REQUEST_LINK = 'http://localhost:8000/request';

export const REGEX_ZIPCODE = /^\d{5}(-\d{4})?$/;

export const REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const REGEX_PHONE = /^((1?)-?\(?(\d{3})\)?-??(\d{3})-?(\d{4}))$/;

export const LIST_STATE = [
  'AL', 'Alabama', 'AK', 'Alaska', 'AZ', 'Arizona', 'AR', 'Arkansas', 'CA', 'California', 'CO', 'Colorado', 'CT', 'Connecticut', 'DE', 'Delaware', 'FL', 'Florida', 'GA', 'Georgia', 'HI', 'Hawaii', 'ID', 'Idaho', 'IL', 'Illinois', 'IN', 'Indiana', 'IA', 'Iowa', 'KS', 'Kansas', 'KY', 'Kentucky', 'LA', 'Louisiana', 'ME', 'Maine', 'MD', 'Maryland', 'MA', 'Massachusetts', 'MI', 'Michigan', 'MN', 'Minnesota', 'MS', 'Mississippi', 'MO', 'Missouri', 'MT', 'Montana', 'NE', 'Nebraska', 'NV', 'Nevada', 'NH', 'New Hampshire', 'NJ', 'New Jersey', 'NM', 'New Mexico', 'NY', 'New York', 'NC', 'North Carolina', 'ND', 'North Dakota', 'OH', 'Ohio', 'OK', 'Oklahoma', 'OR', 'Oregon', 'PA', 'Pennsylvania', 'RI', 'Rhode Island', 'SC', 'South Carolina', 'SD', 'South Dakota', 'TN', 'Tennessee', 'TX', 'Texas', 'UT', 'Utah', 'VT', 'Vermont', 'VA', 'Virginia', 'WA', 'Washington', 'WV', 'West Virginia', 'WI', 'Wisconsin', 'WY', 'Wyoming',
];

export function buildGeocodeUrl(latLong) {
  if (__DEBUG__) console.log('buildGeocodeUrl ' + latLong)
  var lat = 0;
  var lon = 0;
  [lat, lon] = latLong.split(",");
  console.log("lat: ${lat}, lon: ${lon}");
  return _.template(OSM_API_URL)({ lat,lon });
}

export function buildPaths(obj) {
  return Object.keys(obj)
}

export function log(v) {
  if (__DEBUG__) console.log(v)
  return v
}

export function printTable(v) {
  if (__DEBUG__) console.table(v)
  return v
}

export function logWithKey(k, v) {
  console.log("%c" + k, "color:blue;font-weight:bold;font-size: 20px;");
  console.log(v)
  // if (__DEBUG__) console.log(v)
  return v
}

export const samples = {}
global.samples = samples
export function sample(k, v) {
  if (!recordings[k] || !_.isArray(recordings[k])) {
    recordings[k] = []
  }
  return recordings[k].push(v)
}

export function doSampleWithKey(k, v) {
  sample(k, v)
  return v
}

export function doAssignGlobalWithKey(k, v) {
  global[k] = v
  return v
}
