/**
 * # RequestForm.js
 *
 * This class utilizes the ```tcomb-form-native``` library
 */
'use strict';

import React, {
  PropTypes,
  View,
  Component,
} from 'react';

import {
  Text,
  StatusBar,
  Alert,
  TouchableHighlight,
  ScrollView,
} from 'react-native';

import {
  REGISTER,
  LOGIN,
  FORGOT_PASSWORD,
} from './constants';

import {
  buildGeocodeUrl,
  REQUEST_LINK,
  log,
  printTable,
  doSampleWithKey,
  doAssignGlobalWithKey,
  logWithKey,
  REGEX_ZIPCODE,
  REGEX_PHONE,
  LIST_STATE
} from './util';

import styles from './styles';

import _ from 'lodash';
import Button from 'apsl-react-native-button';
import t from 'tcomb-form-native';
const Form = t.form.Form;
const assignG = _.curry(doAssignGlobalWithKey);
const logWithKeyC = _.curry(logWithKey);

export default class RequestForm extends Component {

  /**
   * ## RequestForm class
   *
   * * form: the properties to set into the UI form
   * * value: the values to set in the input fields
   * * onChange: function to call when user enters text
   */
  propTypes: {
    form: PropTypes.object,
    value: PropTypes.object,
    handleChange: PropTypes.func,
    handleFillIn: PropTypes.func,
    handleSubmit: PropTypes.func,
    handleGetCurrentPositionError: PropTypes.func,
  }

  constructor(a,b) {
    super(a,b);

    this._handleFillIn = this.handleFillIn.bind(this);
    this._handleChange = this.handleChange.bind(this);
    this._handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      watchID: null,
      initialPosition: 'unknown',
      lastPosition: 'unknown',
      latitude: null,
      longitude: null,
      value: {
        name: null,
        address: null,
        city: null,
        state: null,
        zip: null,
        phone: null,
        notes: null,
      },
      formType: t.struct({
        name: t.refinement(t.String, a => 0 < a.length),
        address: t.refinement(t.String, a => 0 < a.length),
        city: t.refinement(t.String, a => 0 < a.length),
        state: t.refinement(t.String, s => LIST_STATE.indexOf(s)),
        zip: t.refinement(t.Number, z => String(z).match(REGEX_ZIPCODE)[0]),
        phone: t.refinement(t.Number, p => String(p).match(REGEX_PHONE)[0]),
        notes: t.maybe(t.String),
      }),
      currentStatusBarHeight: StatusBar.currentHeight,
    };

    return this; // end constructor
  }

  /**
   * register functions to poll for the current position
   * @return {void}
   */
  componentDidMount() {
    // N.B. navigator.geolocation.getCurrentPosition returns null
    // it can only be used through the callback params
    navigator.geolocation.getCurrentPosition(
      (position) => {
        // deconstruct
        const {
          coords: {
            latitude,
            longitude,
          }
      } = position;
        // debugger;
        // reconstruct
        this.setState({
          latitude,
          longitude,
        });
      },
      (error) => {
        this._handleGetCurrentPositionError?this._handleGetCurrentPositionError():0;
        return console.error(error.message);
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
      }
    );

    // this.watchID = navigator.geolocation.watchPosition(position => {})
  }

  componentWillUnmount() {
  }

  /**
   * receive the new form struct
   * @return {void}
   */
  handleChange(value) {
    // if (values.male !== this.state.values.male) {
    //   values.female = !this.state.values.female;
    // } else if (values.female !== this.state.values.female) {
    //   values.male = !this.state.values.male;
    // }
    logWithKey('changevalue',value);
    this.setState({value});
  }

  /**
   * attempt to fetch location data and fill in the form
   * @return {void}
   */
  handleFillIn() {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          // deconstruct
          const {
            coords: {
              latitude,
              longitude,
            }
          } = position;
          // debugger;
          // reconstruct
          this.setState({
            latitude,
            longitude,
          });
          global.thing = this;
          console.log('handleFillIn clicked');
          if (!this.state.latitude || !this.state.longitude) {
            console.log('latlong exit early');
            return;
          }
          console.log("lat: "+this.state.latitude+" long:"+this.state.longitude);

          fetch(buildGeocodeUrl(`${this.state.latitude},${this.state.longitude}`)) // dont add a space after this comma
            .then(log)
            // response.text() is a promise that processes the response,
            // it CANNOT be passed directly to JSON.parse!!
            // we are no longer using it but r.json() behaves similarly
            .then(r => r.json())
            .then(jsonResponse => {

              if (jsonResponse.error) {
                // throw new Exception('Could not retrieve address') // TODO: Having problems with this
                console.error('Could not retreive address\nStatus: ' + jsonResponse.error);
                Alert.alert("Address Error","Could not retreive address: " + jsonResponse.error);
              }

              const results = jsonResponse;
              console.log("display_name: "+results.display_name);

              if (!results.display_name) {
                // TODO: FIND+PROCESS MORE THINGS
                Alert.alert("Not ready", "Not ready to combine results");
                console.error('Not ready to combine results yet');
                return;
              }

              const value = {
                ...this.state.value,
                address: `${results.address.house_number || ''} ${results.address.road}`,
                city: results.address.city,
                state: results.address.state,
                zip: results.address.postcode
              }

              this.setState({value})
              return
            })
            .catch((err) => {
              global.caught = err
              logWithKey('handleFillIn catch, caught assigned')
              console.error(err);
            })
        },
        (error) => {
          this._handleGetCurrentPositionError?this._handleGetCurrentPositionError():0;
          return console.error(error.message)
        },
        {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 1000
        }
      );
  }

  /**
   * attempt to send the info
   * @return {void}
   */
  handleSubmit() {
    // const validate_val = this.refs.form.getValue();
    // if (!validate_val) {
    //     Alert.alert("Error", "There were errors.\nSee notes above");
    //     console.log("Errors happened");
    //     return;
    // }

    const requestData = {
      contacts: [{
        fields: _.omit(this.state.value, 'notes'),
        latitude: this.state.latitude,
        longitude: this.state.longitude,
      }],
      name: this.state.value.name,
      notes: this.state.value.notes,
    }
    logWithKey('requestData', requestData)

    fetch(REQUEST_LINK,{
      method: 'POST',
      headers: ({
        'Content-Type': 'application/json',
      //   // 'Content-Length': content.length.toString(),
      //   'X-Custom-Header': 'ProcessThisImmediately',
      }),
      // mode: 'cors',
      // cache: 'default',
      body: JSON.stringify(requestData),
    })
    .then(logWithKeyC('post withoutDecode'))
    .then(r => r.text())
    .then(logWithKeyC('post text'))
    .then(r => JSON.parse(r))
    .then(logWithKeyC('post'))
    .then(data => {
      Alert.alert('Thanks!', 'Your submission has been recorded!');
      this.setState({ value: {
          name: null,
          address: null,
          city: null,
          state: null,
          zip: null,
          phone: null,
          notes: null,
        }
      });
    })
    .catch(err => {
      global.err = err
      logWithKey('err', err)
      Alert.alert('Sorry!', 'There was an error\nThe developer has been notified');
      console.error(err)
    });
  }

  handleGetCurrentPositionError(err) {}

  /**
   * ## render
   *
   * setup all the fields using the props and default messages
   */
  render() {
    let options = {
      auto: 'placeholders',
      fields: {
        zip: {
          // type: 'number'
        },
        phone: {
          // type: 'number',
          error: 'Please enter a phone number'
        }
      }
    };

    return (
      <ScrollView
          automaticallyAdjustContentInsets={false}
          scrollEventThrottle={200}
          directionalLockEnabled={true}
          snapToAlignment={'center'}
          contentContainerStyle={styles.container}>
          <Form ref="form"
            type={this.state.formType}
            options={options}
            value={this.state.value}
            onChange={this._handleChange}
          />
          <Button onPress={this._handleFillIn}>Fill in from Location</Button>
          <Button onPress={this._handleSubmit}>Submit</Button>
      </ScrollView>
    )
  }
}
