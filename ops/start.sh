#!/bin/bash

# composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
if [ -f /var/www/composer.json ]; then
    composer install -d /var/www -vvv
fi

# start supervisor services
supervisord -n -c /etc/supervisord.conf

# Tweak nginx to match the workers to cpu's
procs=$(cat /proc/cpuinfo |grep processor | wc -l)
sed -i -e "s/worker_processes 5/worker_processes $procs/" /etc/nginx/nginx.conf
