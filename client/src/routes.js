import _ from 'lodash'
import React from 'react'
import App from './App'
import Home from './Home'
import CrudList from './CrudList'
import {
    Router,
    Route,
    IndexRoute,
    browserHistory
} from 'react-router'

import {
    BASE_URL,
    loudLog,
} from './util'

const loadModule = (cb) => (componentModule) => {
    cb(null, componentModule.default);
}

export const errorLoading = (err) => {
    console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
}

const fetchConfig = {
    'method': 'GET',
    'headers': new Headers({
        'Content-Type': 'application/json',
        //'Content-Length': content.length.toString(),
        'X-HTTP-Method-Override': 'OPTIONS',
        //'Authorization': 'Bearer YOgwzQdHOt4iwbgRFYCy3WcITQ7YRY5IdUc3Z4qK',
    })
}

export function buildRoutes(base_url, store, cb) {
    loudLog('buildRoutes called')
    fetch(base_url, fetchConfig)
        .catch(err => {
            console.error(err)
        })
        .then(response => response.json())
        .then(schemas => {
                let schemaNames = _(schemas).toPairs().map(p => p[0]).value()
                // console.log('schemasFetched!', schemaNames.length, schemaNames)

                let rs = _.map(schemas, (v, k, a) => {
                    // console.log('defining "' + k + '" as a route')
                    return (
                        <Route key={k} path={k} name={k} component={CrudList}/>
                    )
                })

                let routes = [...rs]

                //debugger;

                // pass the schemas on to the caller
                cb(schemas, (
                    <Route path="/" component={App}>
                        <IndexRoute component={Home} />
                        {routes}
                    </Route>)
                )
            }
        )
}
