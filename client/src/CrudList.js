import React, { Component } from 'react';
import logo from './logo.svg';
//import './CrudList.css';
import { connect } from 'react-redux'
import {
    loudLog,
    toJson,
    titleCase,
} from './util'

import t from 'tcomb-form'

class CrudList extends Component {

    currentPage = 0

    constructor(props) {
        super()
        this.currentPage = 1
    }

    getCurrentSchema(route) {
        route.trim('/')
    }

    onSubmit(evt) {
        evt.preventDefault()
        const value = this.refs.form.getValue()
        if (value) {
            console.log(value)
        }
    }

    static typeToTcombMap = {
        integer: t.Integer,
        string: t.String,
        double: t.Number,
        array: {
            customHandler(prop) {
                return _.map(prop, p => CrudList.buildTcombSchemaFromJsonSchema(p))
            }
        },
        object: {
            customHandler(prop) {
                return _.mapValues(prop, p => CrudList.buildTcombSchemaFromJsonSchema(p))
            }
        }
    }

    static buildTcombSchemaFromJsonSchema(jsonSchema) {

        if (jsonSchema.type == 'object') {
            return t.struct(_.mapValues(jsonSchema.properties, value => {

                if (CrudList.typeToTcombMap[value.type]) {
                    let propType = CrudList.typeToTcombMap[value.type]

                    let propValue = _.isFunction(propType.customHandler) ? propType.customHandler(value) : false
                        // ? propType.customHandler(value)
                        // : propType

                    console.log(propValue, "it's a string! " + (propValue instanceof t.String))
                    loudLog('customHandler? ' + (propValue.customHandler))
                    return t.String

                    return propValue
                    //    //    return [
                    //    //        k,
                    //    //        propFn.isPrototypeOf(Function)
                    //    //            ? propFn(v)
                    //    //            : propFn
                    //    //    ]
                    //        return t.Number
                }
                //
                //    return t.String
            }))

        }
        if (true || !jsonSchema.type)
            return t.struct({})


        const schema = t.struct({
            name: t.String,         // a required string
            age: t.maybe(t.Number), // an optional number
            rememberMe: t.Boolean   // a boolean
        })

        return schema
    }

    static getPage() {
        return fetch(BASE_URL + this.props.route.path)
    }

    render() {
        let currentSchema = this.props.schema[this.props.route.path]

        return (
            <div className="CrudList">
                <h2>WE MADE IT: {titleCase(this.props.route.path)}</h2>
                <form onSubmit={this.onSubmit}>
                    <t.form.Form ref="form" type={CrudList.buildTcombSchemaFromJsonSchema(currentSchema)}/>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        );
    }
}
//<pre>{"state: " + toJson(currentSchema)}</pre>

let mapStateToProps = (state) => {
    let s = state.schema
    console.log(state)
    //debugger
    return {
        routing: state.routing,
        schema: state.schema,
        //currentSchema: state.route[state.schema],
    }
}

//export default CrudList
export default connect(mapStateToProps)(CrudList)
