/**
 * Created by tomas on 7/24/16.
 */
import React, { Component } from 'react';
//import logo from './logo.svg';
//import './NotFound.css';

class NotFound extends Component {
    render() {
        return (
            <div className="NotFound">
                <div className="NotFound-header">
                    <img src={logo} className="NotFound-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <p className="NotFound-intro">
                    You seem to be lost
                </p>
            </div>
        );
    }
}

export default NotFound;
