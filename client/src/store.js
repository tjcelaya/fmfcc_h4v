import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Router, Route, browserHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

export default function configureStore(initialState = {},
                                       history) {
    const reducers = {
        schema: (state = {}, action) => {
            switch (action.type) {
                case 'ASSIGN_SCHEMA':
                    return action.payload
                default:
                    return state
            }
        },
        routing: routerReducer
    }

    const store = createStore(
        combineReducers({
            ...reducers,
        }),
        window.devToolsExtension && window.devToolsExtension()
    )

    return store
}
