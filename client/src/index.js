import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
//import 'babel-polyfill';
import { Provider } from 'react-redux';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import useScroll from 'react-router-scroll';
import configureStore from './store';

// Import the CSS reset, which HtmlWebpackPlugin transfers to the build folder
//import 'sanitize.css/sanitize.css';

//import { install } from 'offline-plugin/runtime';

// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
const initialState = {};
export const store = configureStore(initialState, browserHistory);
const history = syncHistoryWithStore(browserHistory, store)
import { buildRoutes, getCached } from './routes'
import { loudLog, BASE_URL } from './util'

buildRoutes(BASE_URL, store, (schema, rootRoute) => {

    let stuff = {
        type: 'ASSIGN_SCHEMA',
        payload: schema
    };
    console.log(stuff)
    loudLog('rootRoute')
    console.log(rootRoute)
    store.dispatch(stuff)
    //{/*applyRouterMiddleware(useScroll())*/}

    
    ReactDOM.render(
        <Provider store={store}>
            <Router
                history={history}>
                {rootRoute}
            </Router>
        </Provider>,
        document.getElementById('root')
    );
    //routes={rootRoute}

    //Install ServiceWorker and AppCache in the end since
    //it's not most important operation and if main code fails,
    //we do not want it installed

    //install();
})
