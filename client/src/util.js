
export const BASE_URL = 'http://localhost:8000/'

export function loudLog(arg, ...args) {
    console.log("%c" + arg, "color:red; background:white; font-size: 16pt")
    if (args.length) console.log(args)
}

export function toJson (x) { return JSON.stringify(x, null, '\t') }

export function titleCase(str) {
    if ('string' !== typeof str) {
        throw "titleCase only accepts strings"
    }

    return str.split(' ').map(([h, ...t]) => h.toUpperCase() + t.join('').toLowerCase());
}