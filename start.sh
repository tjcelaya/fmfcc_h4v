#!/bin/bash
docker-compose up -d & \
  cd server && echo "running 'make s' in server" && make s & \
  cd client && echo "running 'npm run start' in client" && npm run start
