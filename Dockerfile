FROM ubuntu:14.04
MAINTAINER Dalton Perdue <dalton@cloudbutt.lol>

# keep upstart quiet Bug #571054
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# no tty
ENV DEBIAN_FRONTEND noninteractive

# Get up to date
RUN apt-get update --fix-missing

# Install Build tools
RUN apt-get -y install curl git unzip

# install nginx
RUN apt-get -y install nginx

# install php
RUN apt-get -y install php5-fpm php5-mysql php5-mcrypt php5-curl php5-cli php5-common php-pear php5-json php5-xdebug
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/
RUN mv /usr/bin/composer.phar /usr/bin/composer

# install supervisor
RUN apt-get -y install python-setuptools
RUN easy_install supervisor && easy_install supervisor-stdout

# bit of a tidy up
RUN apt-get autoremove -y && \
echo -n > /var/lib/apt/extended_states && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /usr/share/man/?? && \
rm -rf /usr/share/man/??_*

# file management
ADD ./ops/supervisord.conf /etc/supervisord.conf
ADD ./ops/vhost.conf /etc/nginx/sites-available/default
ADD ./ops/xdebug.ini /etc/php5/mods-available/xdebug.ini
ADD ./ops/php-fpm.ini /etc/php5/fpm/php.ini
ADD ./ops/php-cli.ini /etc/php5/cli/php.ini
ADD ./fmfcc-hv-backend/ /var/www/
RUN chown -Rf www-data:www-data /var/www/
RUN touch /var/log/php5-fpm.log

# expose ports
EXPOSE 80
EXPOSE 443

# --force
RUN echo "alias phpdebug='PHP_IDE_CONFIG=serverName=www.cloudbutt.lol XDEBUG_CONFIG=idekey=xdebug php -c /etc/php5/fpm/php.ini'" >> /etc/bash.bashrc

# rev er up
ADD ./ops/start.sh /start.sh
RUN chmod 755 /start.sh
CMD ["/bin/bash", "/start.sh"]
