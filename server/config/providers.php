<?php

return [

    //                                "
    // m     m  mmm    m mm  m mm   mmm    m mm    mmmm
    // "m m m" "   #   #"  " #"  #    #    #"  #  #" "#
    //  #m#m#  m"""#   #     #   #    #    #   #  #   #
    //   # #   "mm"#   #     #   #  mm#mm  #   #  "#m"#
    //                                             m  #
    //                                              ""
    // maybe they shouldn't be at the top?

    Dingo\Api\Provider\LaravelServiceProvider::class,

    Zizaco\Entrust\EntrustServiceProvider::class,

    NilPortugues\Laravel5\HalJson\Laravel5HalJsonServiceProvider::class,

    Appzcoder\CrudGenerator\CrudGeneratorServiceProvider::class,

    Collective\Html\HtmlServiceProvider::class,

    /*
     * Laravel Framework Service Providers...
     */
    Illuminate\Auth\AuthServiceProvider::class,
    Illuminate\Broadcasting\BroadcastServiceProvider::class,
    Illuminate\Bus\BusServiceProvider::class,
    Illuminate\Cache\CacheServiceProvider::class,
    Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
    Illuminate\Cookie\CookieServiceProvider::class,
    Illuminate\Database\DatabaseServiceProvider::class,
    Illuminate\Encryption\EncryptionServiceProvider::class,
    Illuminate\Filesystem\FilesystemServiceProvider::class,
    Illuminate\Foundation\Providers\FoundationServiceProvider::class,
    Illuminate\Hashing\HashServiceProvider::class,
    Illuminate\Mail\MailServiceProvider::class,
    Illuminate\Pagination\PaginationServiceProvider::class,
    Illuminate\Pipeline\PipelineServiceProvider::class,
    Illuminate\Queue\QueueServiceProvider::class,
    Illuminate\Redis\RedisServiceProvider::class,
    Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
    Illuminate\Session\SessionServiceProvider::class,
    Illuminate\Translation\TranslationServiceProvider::class,
    Illuminate\Validation\ValidationServiceProvider::class,
    Illuminate\View\ViewServiceProvider::class,

    /*
     * Application Service Providers...
     */
    App\Providers\AppServiceProvider::class,
    App\Providers\AuthServiceProvider::class,
    App\Providers\EventServiceProvider::class,
    App\Providers\RouteServiceProvider::class,


    LucaDegasperi\OAuth2Server\Storage\FluentStorageServiceProvider::class,
    LucaDegasperi\OAuth2Server\OAuth2ServerServiceProvider::class,
    App\Providers\OAuthServiceProvider::class,

    Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,

    // Nwidart\LaravelBroadway\LaravelBroadwayServiceProvider::class,

];
