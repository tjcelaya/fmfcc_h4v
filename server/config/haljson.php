<?php

return [
    [
        'class'              => \App\Model\Request::class,
        'alias'              => 'Request',
        'aliased_properties' => [
        ],
        'hide_properties'    => [
        ],
        'id_properties'      => [
            'id',
        ],
        'urls'               => [
            'self' => 'request.show', //named route
        ],
        'curies'             => [
            'name' => 'request',
            'href' => getenv('APP_URL') . '/request',
            'method' => 'OPTIONS'
        ],
    ],
    [
        'class'              => \App\Model\User::class,
        'alias'              => 'User',
        'aliased_properties' => [
        ],
        'hide_properties'    => [
        ],
        'id_properties'      => [
            'id',
        ],
        'urls'               => [
            'self' => 'user.show', //named route
        ],
        'curies'             => [
            'name' => 'user',
            'href' => getenv('APP_URL') . '/user',
            'method' => 'OPTIONS'
        ],
    ],
    [
        'class'              => \App\Model\Contact::class,
        'alias'              => 'Contact',
        'aliased_properties' => [
        ],
        'hide_properties'    => [
        ],
        'id_properties'      => [
            'id',
        ],
        'urls'               => [
            'self' => 'contact.show', //named route
        ],
        'curies'             => [
            'name' => 'contact',
            'href' => getenv('APP_URL') . '/contact',
            'method' => 'OPTIONS'
        ],
    ],
    [
        'class'              => \App\Model\Status::class,
        'alias'              => 'Status',
        'aliased_properties' => [
        ],
        'hide_properties'    => [
        ],
        'id_properties'      => [
            'id',
        ],
        'urls'               => [
            'self' => 'status.show', //named route
        ],
        'curies'             => [
            'name' => 'status',
            'href' => getenv('APP_URL') . '/status',
            'method' => 'OPTIONS'
        ],
    ],
];
