<?php

use App\Model\User;
use App\Model\Request as RequestModel;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MinimumViableModelsTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        // $this->visit('/')
        //      ->see('Laravel 5');

        // $us = get_class_methods($this); sort($us); echo implode($us, "\n");

        $this->assertTrue(true);
    }

    public function testThatUsersExistAndHaveRequestsAssigned()
    {
        $personWithStuff = User::has('requests')->first();
        $this->assertNotNull($personWithStuff);
        $this->assertTrue(0 < $personWithStuff->requests()->count());
    }

    public function testThatRequestsExistAndHaveContacts()
    {
        // requests are assumed to have contact info!
        $req = RequestModel::first();
        $this->assertNotNull($req);
        $this->assertNotNull($req->contacts);
    }
}
