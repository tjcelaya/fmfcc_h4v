<?php

/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 7/16/16
 * Time: 7:51 PM
 */
class AuthorizationWorksTest extends TestCase
{
    public function testBadRequestOnUnauthorized()
    {
        $rez      = $this->call('GET', '/request');
        $strRes   = (string)$rez->getContent();
        $response = json_decode($strRes, true);
        $this->assertArrayNotHasKey('data', $response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('status_code', $response);
        $this->assertEquals($response['status_code'], 400);
        $this->assertResponseStatus(400);
    }

    public function testRootCredentials()
    {
        $h = [
            'Authorization' => 'Bearer ' . ($rootBearerToken = DB::table('oauth_access_tokens')->value('id')),
            'Content-Type'  => 'application/json',
        ];

        $this->json('GET', '/request', [], $h)
             ->seeJsonStructure(
                 [
                     'data' =>
                         [
                             '*' => [
                                 'id',
                                 'name',
                                 'notes',
                             ],
                         ],
                     'meta' => [
                         'pagination' => [
                             'total',
                             'count',
                             'per_page',
                             'current_page',
                             'total_pages',
                             'links',
                         ],
                     ],
                 ]
             );

        $this->assertResponseOk();
    }
}
