php artisan crud:generate Posts --fields="title:string, body:text"

php artisan crud:generate Type       --fields="name:string, kind:string"
php artisan crud:generate Contact    --fields="fields:text"
php artisan crud:generate User       --fields="name:string, email:string, password:string"            
php artisan crud:generate Request    --fields="name:string, notes:text"        
php artisan crud:generate Resource   --fields="name:string"        
php artisan crud:generate Status     --fields="name:string"          
