@extends('layouts.master')

@section('content')

    <h1>Post</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Fields</th><th>Raw</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $post->id }}</td> <td> {{ $post->fields }} </td><td> {{ $post->raw }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection