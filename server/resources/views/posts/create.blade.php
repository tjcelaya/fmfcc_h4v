@extends('layouts.master')

@section('content')

    <h1>Create New Post</h1>
    <hr/>

    {!! Form::open(['url' => 'posts', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('fields') ? 'has-error' : ''}}">
                {!! Form::label('fields', 'Fields: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('fields', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('fields', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('raw') ? 'has-error' : ''}}">
                {!! Form::label('raw', 'Raw: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('raw', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('raw', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection