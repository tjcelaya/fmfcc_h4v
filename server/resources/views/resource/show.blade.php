@extends('layouts.master')

@section('content')

    <h1>Resource</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $resource->id }}</td> <td> {{ $resource->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection