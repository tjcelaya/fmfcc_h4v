@extends('layouts.master')

@section('content')

    <h1>Type</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Kind</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $type->id }}</td> <td> {{ $type->name }} </td><td> {{ $type->kind }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection