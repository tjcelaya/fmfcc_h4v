@extends('layouts.master')

@section('content')

    <h1>Request</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Notes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $request->id }}</td> <td> {{ $request->name }} </td><td> {{ $request->notes }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection