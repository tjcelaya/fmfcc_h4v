<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Database\Events\QueryExecuted' => [
            'App\Listeners\DBLoggingListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        $events->listen('cache.hit', function ($key, $value) {
            Log::info('cache.hit.' . $key);
        });

        $events->listen('cache.missed', function ($key) {
            Log::info('cache.missed.' . $key);
        });

        $events->listen('cache.write', function ($key, $value, $minutes) {
            Log::info('cache.write.' . $key);
        });

        $events->listen('cache.delete', function ($key) {
            Log::info('cache.delete.' . $key);
        });
    }
}
