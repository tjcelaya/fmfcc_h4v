<?php
namespace App\Providers;

use App\Model\User;
use App\Model\Client;
use Dingo\Api\Auth\Auth;
use Dingo\Api\Auth\Provider\OAuth2;
use Illuminate\Support\ServiceProvider;

class OAuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        \Log::debug('oauth booting');
        $this->app[Auth::class]->extend('oauth', function ($app) {
            $provider = new OAuth2($app['oauth2-server.authorizer']->getChecker());

            $provider->setUserResolver(function ($id) {
                \Log::debug('setUserResolver!');
                return User::find($id);
            });

            $provider->setClientResolver(function ($id) {
                \Log::debug('setClientResolver!');
                return Client::find($id);
            });

            return $provider;
        });
    }

    public function register()
    {
        //
    }
}