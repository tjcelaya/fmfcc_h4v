<?php

namespace App\Providers;

use App;
use Log;
use Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // app(\Dingo\Api\Auth\Auth::class)->extend('basic', function ($app) {
        //     return new \Dingo\Api\Auth\Provider\Basic($app['auth'], 'email');
        // });

        $requestString = Request::method() . ' ' . Request::fullUrl();

        Log::debug(
            ' >>> ' . $requestString,
            Request::input());

        $t = app('Dingo\Api\Transformer\Factory');

        $t->register('App\Model\Request', 'App\Transformer\Base');

        App::terminating(function() use ($requestString) {
            Log::debug(' <<< ' . $requestString);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // recommended way of adding service provider, there is no reason to be
		// "generating" models in production
		if ($this->app->environment() == 'local') {
				$this->app->register('Iber\Generator\ModelGeneratorProvider');
		}

    }
}
