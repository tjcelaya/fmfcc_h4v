<?php

namespace App\Http\Middleware;

use Closure;

class OverrideHTTPMethodByHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headerOverride = $request->header('X-HTTP-Method-Override');

        if ($request->method() === 'GET'
            && $headerOverride === 'OPTIONS'
        ) {
            $request->setMethod($headerOverride);
        }

        return $next($request);
    }
}
