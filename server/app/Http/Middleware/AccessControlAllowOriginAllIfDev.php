<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Dingo\Api\Http\Response;
use Log;

class AccessControlAllowOriginAllIfDev
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        $env=App::environment();
        if ('local' === $env) {
            $response->header('Access-Control-Allow-Origin', '*');
            $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');
            $response->header('Access-Control-Allow-Headers', 'Accept, Content-Type, X-HTTP-Method-Override');
        }

        return $response;
    }
}
