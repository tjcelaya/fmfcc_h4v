<?php

namespace App\Http\Controllers\Auth;

class OauthController extends Controller
    protected function perform() {
        return Response::json(Authorizer::issueAccessToken());
    }
}
