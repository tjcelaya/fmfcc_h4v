<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\v1\CrudController;

use App\Model\Resource;

class ResourceController extends CrudController
{
    protected $model = Resource::class;
}
