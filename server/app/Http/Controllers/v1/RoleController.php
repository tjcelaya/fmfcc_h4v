<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\v1\CrudController;

use App\Model\Role;

class RoleController extends CrudController
{
    protected $model = Role::class;
}
