<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\v1\CrudController;

use App\Model\Permission;

class PermissionController extends CrudController
{
    protected $model = Permission::class;
}
