<?php

namespace App\Http\Controllers\v1;

use App\Http\Requests;
use App\Http\Controllers\v1\CrudController;

use App\Model\Contact;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class ContactController extends CrudController
{
    protected $model = Contact::class;
}
