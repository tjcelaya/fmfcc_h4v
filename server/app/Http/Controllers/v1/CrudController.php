<?php

namespace App\Http\Controllers\v1;

use App\Model\Base;
use Config;
use DB;
use Exception;
use Illuminate\Cache\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log;
use NilPortugues\Api\HalJson\HalJsonTransformer;
use NilPortugues\Api\Mapping\Mapper;
//use NilPortugues\Laravel5\HalJson\Mapper\Mapper;
use NilPortugues\Laravel5\HalJson\HalJsonSerializer;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use NilPortugues\Laravel5\JsonApi\JsonApiSerializer;
use NilPortugues\Laravel5\HalJson\HalJsonResponseTrait;
use Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CrudController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use HalJsonResponseTrait;

    /**
     * @var JsonApiSerializer
     */
    protected $serializer;

    public function __construct(JsonApiSerializer $serializer)
    {
        // $mapper = new Mapper(Config::get('haljson'));
        // $this->serializer = new HalJsonSerializer(new HalJsonTransformer($mapper));
        $f = app('Dingo\Api\Transformer\Factory')->register($this->model, 'App\Transformer\Base');
    }

    protected function shouldLoadRelation($linksRequested, $name, $relationSpec)
    {
        return (
            ($autoloaded = (isset($relationSpec['autoload']) && $relationSpec['autoload']))
            ||
            ($stringRequested = ($linksRequested == 'all' || $linksRequested == $name))
            ||
            ($arrayRequested = (is_array($linksRequested) && in_array($name, $linksRequested)))
        );
    }

    protected function naiveRouteFromClass($klass)
    {
        return snake_case(last(explode('\\', $klass)));
    }

    public function index(HttpRequest $request)
    {
        // Log::info('resource owner'.\Authorizer::getResourceOwnerId());

        $klass = $this->model;

        $thingsQ = $klass::query();

        if ($request->has('filter')) {
            $thingsQ = $this->processFilter($thingsQ, $request->input('filter'));
        }
        if ($request->has('sort')) {
            $thingsQ = $this->processSort($thingsQ, $request->input('sort'));
        }
        if ($request->has('mscope')) {
            $thingsQ = $this->processMscope($thingsQ, $request->input('mscope'));
        }

        $things = $thingsQ->paginate();

        $linksRequested = $request->input('links');
        foreach ($klass::$availableRelations as $name => $relationSpec) {
            if ($this->shouldLoadRelation($linksRequested, $name, $relationSpec)) {
                $things->load($name);
            }
        }

        return $things;
        /** @var \NilPortugues\Api\HalJson\HalJsonTransformer $transformer */
        // $transformer = $this->serializer->getTransformer();
        // $transformer->setSelfUrl(route($this->naiveRouteFromClass($klass) . '.index'));
        // return $this->response($this->serializer->serialize($things));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->model . '.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(HttpRequest $request)
    {
        $klass = $this->model;
        Log::debug($klass, $request->input());

        DB::beginTransaction();

        $thing = new $klass($request->input());
        $thing->save();

        try {
            $relations = $this->attachRelations($thing, $request);
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();
            abort(400, 'Failed to attach related data');
        }

        DB::commit();
        logn();

        if (class_exists($klassCreated = 'App\Events\\' . studly_case($klass) . 'Created')) {
            event(new $klassCreated($thing));
        }

        return $thing;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        logy();
        $klass = $this->model;
        $thing = $klass::find($id);

        if (!$thing) {
            abort(404, last(explode('\\', $klass)) . ' not found');
        }

        $linksRequested = Request::get('links');
        foreach ($klass::$availableRelations as $name => $relationSpec) {
            if ($this->shouldLoadRelation($linksRequested, $name, $relationSpec)) {
                $thing->load($name);
            }
        }
        logn();

        return $thing;
        /** @var \NilPortugues\Api\HalJson\HalJsonTransformer $transformer */
        // $transformer = $this->serializer->getTransformer();
        // $transformer->setSelfUrl(route($this->naiveRouteFromClass($klass) . '.show', ['id' => $id]));
        // return $this->response($this->serializer->serialize($thing));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view($this->model . '.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(HttpRequest $request, $id)
    {
        $klass = $this->model;
        $thing = $klass::findOrFail($id);
        $input = $request->input();
        $thing->update(isset($input['_embedded']) ? $this->extractEmbedded($input) : $input);

        return $thing;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $klass = $this->model;
        $thing = $klass::findOrFail($id);
        $thing->delete();

        return $thing;
    }

    public function options(HttpRequest $request, Repository $cache)
    {
        /** @var Base $m */
        $klass = $this->model;
        $cacheKey = $klass . ':options';

        if ($cache && $cache->has($cacheKey)) {
            Log::info("found $cacheKey in cache");
            return $cache->get($cacheKey);
        }

        $fields = [];

        $m = $klass::first(); // TODO: figure out how to do this without DB call

        // array union by key
        // $fields = array_flip($m->getFillable()) + array_keys($klass::$availableRelations);

        $this->addSchemaForObject($klass, $m, $fields);

        $fields['title'] = studly_case($this->naiveRouteFromClass($klass));
        $fields['path']  = $request->path();

        if ($cache) {
            Log::info("putting $cacheKey in cache");
            $cacheKey = $klass . ':options';
            $cache->put($cacheKey, $fields, 60);
        }

        return $fields;
    }

    protected function attachRelations($thing, HttpRequest $request)
    {

        $klass = $this->model;

        // array_intersect_key takes the key,value pairs from the first array
        // that have keys present in all other arguments provided.
        // quickly.

        $matchingKeys = array_intersect_key($request->input(), $klass::$availableRelations);

        foreach ($matchingKeys as $name => $relationData) {
            if (!isset($klass::$availableRelations[$name])) {
                Log::info(
                    'exiting early because ' . $name . ' is not in ' . var_export(
                        array_keys($klass::$availableRelations),
                        true
                    )
                );
                continue;
            }

            Log::debug('saving relations of this kind of thing -> ', $klass::$availableRelations[$name]);

            if ($klass::$availableRelations[$name]['cardinality'] == 'many') {
                $relatedThings = new Collection;

                Log::info('many ' . var_export($relationData, true));

                foreach ($relationData as $relatedThingData) {
                    Log::info(
                        'making a ' .
                        $klass::$availableRelations[$name]['class'] .
                        ' from this data: ' .
                        json_encode($relatedThingData)
                    );
                    $relatedThing = new $klass::$availableRelations[$name]['class']($relatedThingData);
                    $relatedThing->save();
                    $relatedThings[] = $relatedThing;
                }

                Log::info('going to save some ' . $thing->$name . var_export($relationData, true));

                $attachResult = $thing->{$name}()->saveMany($relatedThings);
                $thing->load($name);
            } else {
                throw new Exception(
                    'Unexpected cardinality: ' .
                    $klass::$availableRelations[$name]['cardinality'] .
                    ' for relationship ' .
                    $klass .
                    ' -> ' .
                    $klass::$availableRelations[$name]['class']
                );
            }
        }
    }

    private function extractEmbedded(&$input)
    {
        if (empty(($input['_embedded'])) || !is_array($input['_embedded'])) {
            return;
        }

        foreach ($input['_embedded'] as $ek => $eV) {
            $input[$ek] = $eV;
        }

        unset($input['_embedded']);

        return $input;
    }

    private function addSchemaForObject($klass, $instance, &$fields)
    {
        $fields['type'] = 'object';

        foreach ($instance->getFillable() as $prop) {
            $fields['properties'][$prop] = [
                'name'        => $prop,
                'description' => isset($klass::$jsonSchema[$prop]['description']) ? $klass::$jsonSchema[$prop]['description'] : $prop,
                'type'        => gettype($instance->{$prop}),
            ];
        }

        foreach ($klass::$availableRelations as $name => $relationSpec) {
            $relatedKlass = $relationSpec['class'];
            $rM           = $relatedKlass::firstOrFail(); // TODO: figure out how to do this without DB call

            $fields['properties'][$name] = [
                'type' => $relationSpec['cardinality'] == 'many' ? 'array' : 'object',
                //                array_flip($rM->getFillable()),
            ];

            if ($relationSpec['cardinality'] == 'many') {
                $this->addSchemaForObject($relatedKlass, $rM, $fields['properties'][$name]['items']);
            } else {
                $this->addSchemaForObject($relatedKlass, $rM, $fields['properties'][$name]);
            }
        }
    }

    const OPS = ['=', '<=', '<', '>=', '>', '<>', 'like'];

    /**
     *
     * Process $input that looks like ?filter[id]=1
     *
     * @param Builder $thingsQ
     * @param array   $input
     *
     * @return Builder
     */
    private function processFilter(Builder $thingsQ, array $input)
    {
        foreach ($input as $fieldKey => $fieldCompare) {
            // list($thing, $relatedThings) = explode('.', $fieldKey);

            if (mb_strpos($fieldCompare, '.', 0, 'UTF-8')) {
                abort(400, 'No nested property access!');
            }

            if (mb_strpos($fieldCompare, ':', 0, 'UTF-8')) {
                list($op, $val) = explode(':', $fieldCompare, 2);
            } else {
                $op  = '=';
                $val = $fieldCompare;
            }


            if (empty($op)) {
                // filter[$fK]=$val
                $this->where($fieldKey, $op);
            } elseif (in_array(strtolower($op), self::OPS) && mb_strlen($val)) {
                // filter[$fK]=$op:$val
                $thingsQ->where($fieldKey, $op, $val);
            } elseif ($op === 'in') {
                if (strpos($val, ',', 0)
                    && ($vals = explode(',', $val))
                ) {
                    // filter[$fK]=in:1,2,3
                    $thingsQ->whereIn('id', $vals);
                } else {
                    // filter[$fK]=in:1
                    $thingsQ->where('id', $val);
                }
            } elseif ($op === 'has') {
                if (strpos($val, ',', 0)
                    && ($vals = explode(':', $val, 2))
                ) {
                    list($comparisonOperator, $count) = $vals;
                    // filter[$fK]=has:>=:1
                    $thingsQ->has($fieldKey, $comparisonOperator ?: '>=', $count ?: 1);
                    null;
                } else {
                    // filter[$fK]=has
                    $thingsQ->has($fieldKey);
                }
            } elseif ($op === 'isnotnull') {
                // filter[$fK]=isnotnull
                $thingsQ->whereNotNull($fieldKey);
            } elseif ($op === 'isnull') {
                // filter[$fK]=isnull
                $thingsQ->whereNull($fieldKey);
            }
        }

        return $thingsQ;
    }

    /**
     *
     * http://jsonapi.org/format/#fetching-sorting
     * An endpoint MAY support requests to sort the primary data with a sort query parameter. The value for sort MUST
     * represent sort fields. An endpoint MAY support multiple sort fields by allowing comma-separated (U+002C COMMA,
     * “,”) sort fields. Sort fields SHOULD be applied in the order specified. The sort order for each sort field MUST
     * be ascending unless it is prefixed with a minus (U+002D HYPHEN-MINUS, “-“), in which case it MUST be descending.
     *
     * @param Builder $thingsQ
     * @param         $input
     *
     * @return Builder
     */
    private function processSort(Builder $thingsQ, $input)
    {
        foreach (explode(',', $input) as $field) {
            $dir = 'ASC';
            if (mb_strpos($field, '-') !== false) {
                $field = substr($field, 1);
                $dir   = 'DESC';
            }
            $thingsQ->orderBy($field, $dir);
        }

        return $thingsQ;
    }

    private function processMscope($thingsQ, $input)
    {
        // call scope functions after some validation maybe?
        return $thingsQ;
    }
}
