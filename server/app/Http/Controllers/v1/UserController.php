<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\v1\CrudController;

use App\Model\User;

class UserController extends CrudController
{
    protected $model = User::class;
}
