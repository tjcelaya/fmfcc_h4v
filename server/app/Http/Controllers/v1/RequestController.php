<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request as HttpRequest;
use App\Http\Controllers\v1\CrudController;

use App\Model\Request;

class RequestController extends CrudController
{
    protected $model = Request::class;
}
