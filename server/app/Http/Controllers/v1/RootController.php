<?php

namespace App\Http\Controllers\v1;


use Authorizer;
use Dingo\Api\Routing\Helpers;
use App\Http\OptionsEnabledDingoDispatcher;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request as HttpRequest;
use Response;

class RootController extends BaseController
{

    public function getIndex()
    {
        return view('stats');
    }

    public function optionsIndex(HttpRequest $request, OptionsEnabledDingoDispatcher $dispatcher)
    {
        $optionsSchemas = [];
        foreach (routes('OPTIONS') as $route) {
            if (!$route) {
                // skip _this_ route
                continue;
            }

            $optionsSchemas[$route] = $dispatcher->options($route);
        }

        return $optionsSchemas;
    }

    public function postAccessToken()
    {
        return Response::json(Authorizer::issueAccessToken());
    }
}