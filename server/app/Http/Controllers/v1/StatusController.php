<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\v1\CrudController;

use App\Model\Status;

class StatusController extends CrudController
{
    protected $model = Status::class;
}
