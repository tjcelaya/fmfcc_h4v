<?php

APIRoute::version(
    'v1',
    function () {
        APIRoute::post(
            'oauth/access_token',
            [
                'as'   => 'oauth.access_token',
                'uses' => App\Http\Controllers\v1\RootController::class . '@postAccessToken',
            ]
        );

        APIRoute::get(
            '/',
            [
                'as'   => 'root.index',
                'uses' => App\Http\Controllers\v1\RootController::class . '@getIndex',
            ]
        );

        APIRoute::options(
            '/',
            [
                'as'   => 'root.options',
                'uses' => App\Http\Controllers\v1\RootController::class . '@optionsIndex',
            ]
        );

        APIRoute::post(
            '/request',
            [
                'as'   => 'request.store',
                'uses' => 'App\Http\Controllers\v1\RequestController@store',
            ]
        );
    }
);

foreach (get_things() as $thing) {

    $controllerClassPrefix = 'App\Http\Controllers\v1\\' . studly_case($thing);

    APIRoute::version(
        'v1',
        function ($api) use ($thing, $controllerClassPrefix) {
            APIRoute::options(
                '/' . $thing,
                [
                    'as'     => $thing . '.options',
                    'uses'   => $controllerClassPrefix . 'Controller@options',
//                    'scopes' => $thing,
                ]
            );
        }
    );


    APIRoute::version(
        'v1',
        ['middleware' => 'oauth:' . $thing],
        function ($api) use ($thing, $controllerClassPrefix) {

            APIRoute::get(
                $thing,
                [
                    'as'     => $thing . '.index',
                    'scopes' => $thing . '.index',
                    'uses'   => $controllerClassPrefix . 'Controller@index',
                ]
            );

            // we don't want to define the POST /request route, so we'll
            $thing == 'request' ? null : APIRoute::post(
                $thing,
                [
                    'as'     => $thing . '.store',
                    'scopes' => $thing . '.store',
                    'uses'   => $controllerClassPrefix . 'Controller@store',
                ]
            );
            APIRoute::get(
                $thing . '/{id}',
                [
                    'as'     => $thing . '.show',
                    'scopes' => $thing . '.show',
                    'uses'   => $controllerClassPrefix . 'Controller@show',
                ]
            );
            APIRoute::put(
                $thing . '/{id}',
                [
                    'as'     => $thing . '.update',
                    'scopes' => $thing . '.update',
                    'uses'   => $controllerClassPrefix . 'Controller@update',
                ]
            );
            APIRoute::patch(
                $thing . '/{id}',
                [
                    'as'     => $thing . '.update',
                    'scopes' => $thing . '.update',
                    'uses'   => $controllerClassPrefix . 'Controller@update',
                ]
            );
            APIRoute::delete(
                $thing . '/{id}',
                [
                    'as'     => $thing . '.destroy',
                    'scopes' => $thing . '.destroy',
                    'uses'   => $controllerClassPrefix . 'Controller@destroy',
                ]
            );
        }
    );
}
