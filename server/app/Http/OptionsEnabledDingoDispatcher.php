<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 7/24/16
 * Time: 12:33 PM
 */

namespace App\Http;

use Dingo\Api\Dispatcher;

class OptionsEnabledDingoDispatcher extends Dispatcher
{
    public function options($uri, $parameters = [])
    {
        return $this->queueRequest('options', $uri, $parameters);
    }
}