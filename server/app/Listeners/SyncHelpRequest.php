<?php

namespace App\Listeners;

use App\Events\HelpRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncHelpRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        dd('SyncHelpRequest start');
    }

    /**
     * Handle the event.
     *
     * @param  HelpRequest  $event
     * @return void
     */
    public function handle(HelpRequest $event)
    {
        //
    }
}
