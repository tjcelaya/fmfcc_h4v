<?php

namespace App\Jobs;

use Log;
use App\Jobs\Base;
use App\Model\Contact;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DoGeocodingLookup extends Base implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $contact;

    public function __construct(Contact $c)
    {
        $this->contact = $c;
        return $this;
    }

    public function handle()
    {
        \Log::debug('Beginning DoGeocodingLookup');
        // // check the attemts
        // if ($this->attempts() > 3) {}
        // // give up early
        // $this->release(10);

        // do a geocoding lookup with $this->contact

        $key = env('GOOGLE_MAPS_API_KEY');

        if (!$key) {
            $this->release(10);
            return;
        }

        $url = 'https://maps.googleapis.com/maps/api/geocode/json'
          . '?latlng='  . $this->contact->latitude . ',' . $this->contact->latitude
          . '&key=' . env('GOOGLE_MAPS_API_KEY');

        // $this->contact
    }
}
