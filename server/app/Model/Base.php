<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

abstract class Base extends Model
{
    public static $availableRelations = [];
    public static $jsonSchema = [];
}
