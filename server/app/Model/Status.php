<?php

namespace App\Model;
use App\Model\Base;
{

    class Status extends Base
    {

        protected $table = 'status';

        public $timestamps = true;

        protected $fillable = [
            'name',
            'path',
            // 'type',
        ];

        protected $guarded = [];

        // public static function boot() {
        //     if (App::environment() == 'testing' and 0 === self::count()) {
        //         foreach (Lorem::words() as $w) {
        //             (new self(['name' => $w]))->save();
        //         }
        //     }
    // }
    }

}

namespace App\Model\Status;
{
    interface Type {
        const REQUEST = 1;
        const USER = 2;
    }
}