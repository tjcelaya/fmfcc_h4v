<?php

namespace App\Model;

use App\Model\AuthenticatableBase;
use App\Model\Contact;
use App\Model\Request;
use App\Model\Type;
use App\Model\User\Admin;
use App\Model\User\Anon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends AuthenticatableBase
{
    public static $descriptions = [];
    protected $table = 'user';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    // use SingleTableInheritanceTrait;

    // // protected static $singleTableType = 0;
    // protected static $singleTableTypeField = 'type_id';
    // protected static $singleTableSubclasses = [
    //     Anon::class,
    //     Admin::class,
    //     // More dynamic thing?
    // ];

    // protected static $sudo = false;

    public function hasSudo() {
        return $this->sudo;
    }

    public static $availableRelations = [
        'requests' => [
            'class' => Request::class,
            'cardinality' => 'many',
            'autoload' => false,
        ],
        'contacts' => [
            'class' => Contact::class,
            'cardinality' => 'many',
            'autoload' => false,
        ],
        'type' => [
            'class' => Type::class,
            'cardinality' => 'one',
            'autoload' => true,
        ]
    ];

    public function requests() {
        return $this->belongsToMany('App\Model\Request', 'request_user', 'user_id', 'request_id');
    }

    public function contacts() {
        return $this->belongsToMany('App\Model\Contact');
    }

    public function type() {
        return $this->hasOne('App\Model\Type');
    }

    use SoftDeletes, EntrustUserTrait {
        SoftDeletes::restore as untrash;
        EntrustUserTrait::restore as euRestore;
    }

    public function restore() {
        $this->untrash();
        $this->euRestore(); // TODO step into euResotre and see what it does
    }

    // EntrustUserTrait
    // roles()
    // hasRole($name)
    // can($permission)
    // ability($roles, $permissions, $options)
}
