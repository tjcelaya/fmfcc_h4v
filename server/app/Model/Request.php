<?php

namespace App\Model;

use App\Model\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Request
 */
class Request extends Base
{
    use SoftDeletes;
    // use TemporarilyAssignableTrait;

    protected $table = 'request';

    public $timestamps = true;

    protected $visible = [
        'id',
        'name',
        'notes',
        'user',
        'users',
        'contacts',
        'resources',
        'statusHistory',
        'statuses',
    ];

    protected $fillable = [
        'name',
        'notes',
    ];

    // TODO: causes extra update queries! NEEDS TESTING
    // protected $touches = [
    //     // 'users',
    //     'contacts',
    //     // 'resources',
    //     // 'statuses',
    // ];

    public static $availableRelations = [
        'contacts' => [
            'class' => 'App\Model\Contact',
            'cardinality' => 'many',
            'autoload' => true,
        ]
    ];

    protected $guarded = [];
    // protected $appends = ['contacts'];

    public function users() {
        return $this->belongsToMany('App\Model\User', 'request_user', 'request_id', 'user_id')
            ->withTimestamps();
    }

    public function contacts() {
        return $this->belongsToMany('App\Model\Contact')
            ->withTimestamps();
    }

    public function resources() {
        return $this->belongsToMany('App\Model\Resource')
            ->withTimestamps();
    }

    // public function statusHistory() {
    //     return $this->belongsToMany('App\Model\Status')
    //         ->withTimestamps();
    // }

    public function statuses() {
        return $this->belongsToMany('App\Model\Status')
          ->withTimestamps()
          ->whereNull('request_status.deleted_at');
    }
}
