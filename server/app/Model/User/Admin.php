<?php

namespace App\Model\User;

use App\Model\User as BaseUser;

class Admin extends BaseUser {
    protected static $sudo = true;

    protected static $singleTableType = 1;
}