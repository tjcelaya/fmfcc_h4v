<?php

namespace App\Model;
use App\Model\Base;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

class Type extends Base
{
    const CONTACT = 1;
    const REQUEST = 2;
    const RESOURCE = 3;
    const STATUS = 4;
    const TYPE = 5;
    const USER = 6;

    protected $table = 'type';

    public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    // use SingleTableInheritanceTrait;

    // protected static $singleTableType = 0;

    // protected static $singleTableTypeField = 'type_id';

    // protected static $singleTableSubclasses = [
    //     UserType::class,
    //     ContactType::class,
    //     // More dynamic thing?

    //     // type trees...?
    // ];
}

// class User extends Base {
//     protected static $singleTableType = 0;
// }

// class Contact extends Base {
//     protected static $singleTableType = 1;
// }