<?php

namespace App\Model;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    // table name must be configured in `config/entrust.php`

    /** @var string name
      * Unique name for the Role, used for looking up role information in the application layer.
      * For example: "admin", "owner", "employee".
      */
    /** @var string|null display_name
      * Human readable name for the Role. Not necessarily unique and optional.
      * For example: "User Administrator", "Project Owner", "Widget Co. Employee".
      */
    /** @var string|null description
      * A more detailed explanation of what the Role does. Also optional.
      */
}
