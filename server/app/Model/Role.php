<?php

namespace App\Model;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{

    /** @var string name
      * Unique name for the Role, used for looking up role information in the application layer. For example: "admin", "owner", "employee".
      */
    /** @var string display_name
      * Human readable name for the Role. Not necessarily unique and optional. For example: "User Administrator", "Project Owner", "Widget Co. Employee".
      */
    /** @var string description
      * A more detailed explanation of what the Role does. Also optional.
      */

    public function permissions() {
        return $this->belongsToMany(\App\Model\Permission::class);
    }
}
