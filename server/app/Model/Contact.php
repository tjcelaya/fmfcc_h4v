<?php

namespace App\Model;

use App\Model\Base;
use App\Jobs\DoGeocodingLookup;

/**
 * Class Contact
 */
class Contact extends Base
{
    protected $table = 'contact';

    public $timestamps = true;

    protected $fillable = [
        'fields',
        'latitude',
        'longitude',
        'image_url',
    ];

    protected $casts = [
        'fields' => 'array',
        'latitude' => 'double',
        'longitude' => 'double',
    ];

}

Contact::saved(function ($c) {
    if (!$c->latitude ||
        !$c->longitude ||
        (
            $c->latitude && $c->longitude && empty($c->fields)
        )) {
        dispatch(new DoGeocodingLookup($c));
    }
});