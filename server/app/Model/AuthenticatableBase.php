<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

abstract class AuthenticatableBase extends Authenticatable
{
    public static $availableRelations = [];
    public static $jsonSchema = [];
    // protected $dateFormat = 'c';

    // Authenticatable extends laravel model
}
