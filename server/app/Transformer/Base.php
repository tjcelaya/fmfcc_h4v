<?php

namespace App\Transformer;

use Dingo\Api\Routing\UrlGenerator;
use League\Fractal\TransformerAbstract;
use App\Model\Base as BaseModel;

class Base extends TransformerAbstract
{
    private function getShortClassName($klass)
    {
        return strtolower(str_replace('App\\Model\\', '', $klass));
    }

    /**
     * Turn this item object into a generic array
     *
     * @return array|\Illuminate\Database\Eloquent\Model
     */
    public function transform(BaseModel $thing)
    {
        /** @var UrlGenerator $urlGenerator */
        $urlGenerator = app('api.url')->version('v1');

        $klass = get_class($thing);

        if (false === strpos($klass, 'App\\Model\\')) {
            return $thing;
        }

        $resourceName = $this->getShortClassName($klass);
        $thing = $thing->toArray();
        $thing['_links'] = [

            'self' => $urlGenerator->route($resourceName . '.show', $thing['id'])
        ];

        foreach ($klass::$availableRelations as $name => $relationSpec) {
            if (isset($thing['_links'][$name])) {
                abort(500, 'Overlapping relation names?');
            }


            if ($relationSpec['cardinality'] == 'many') {
                if (!isset($thing[$name])) continue;

                foreach ($thing[$name] as &$relatedThing) {
                    $relatedThing['_links']['self'] = $urlGenerator->route($this->getShortClassName($relationSpec['class']) . '.show', $thing['id']);
                }

                // $thing['links'][$name] = [
                //     $this->getShortClassName($relationSpec['class']) => [
                //         $thing['contacts']
                //     ];
                // ];
            }
        }

        return $thing;
    }

}
