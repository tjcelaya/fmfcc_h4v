<?php

use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var Factory $factory */
$factory->define(App\Model\User::class, function (Faker\Generator $f) {
    return [
        'name' => $f->name,
        'type_id' => rand(1,2),
        'email' => $f->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Model\Contact::class, function (Faker\Generator $f) {
    return [
            'fields' => [
                'street' => coinflip() ? $f->streetAddress : null,
                'city' => coinflip() ? $f->city : null,
                'postcode' => coinflip() ? $f->postcode : null,
                'state' => coinflip() ? $f->state : null,
                'email' => coinflip() ? $f->email : null,
            ],
            'latitude' => 0,
            'longitude' => 0,
            'image_url' => 'https://placekitten.com/' . rand(280,320) . '/' . rand(280,320),
            'created_at' => $f->dateTime,
            'updated_at' => $f->dateTime,
            // softDeletes();
    ];
});

$factory->define(App\Model\Type::class, function (Faker\Generator $f) {
    return [
            'name' => $f->word,
            'created_at' => $f->dateTime,
            'updated_at' => $f->dateTime,
            // softDeletes();
    ];
});

$factory->define(App\Model\Status::class, function (Faker\Generator $f) {
    return [
            'name' => $f->word,
            'created_at' => $f->dateTime,
            'updated_at' => $f->dateTime,
            // softDeletes();
    ];
});

$factory->define(App\Model\Request::class, function (Faker\Generator $f) use ($factory) {
    return [
            // $this->relate($t, 'request_status');
            // 'request_status_id' => rand(1,3),
            'name' => $f->name,
            'notes' => $f->text, // nullable
            'created_at' => $f->dateTime,
            'updated_at' => $f->dateTime,
            // softDeletes();
    ];
});

$factory->define(App\Model\Resource::class, function (Faker\Generator $f) {
    return [
            'name' => $f->word,
            'created_at' => $f->dateTime,
            'updated_at' => $f->dateTime,
            // softDeletes();
    ];
});

// $factory->define(App\Model\PasswordResets::class, function (Faker\Generator $f) {
//     return [
//             'email' => $f->word,
//             'token' => $f->word,
//             'created_at' => $f->dateTime,
//     ];
// });
