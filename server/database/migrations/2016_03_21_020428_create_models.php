<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModels extends Migration
{
    protected function relate(
        Blueprint $t,
        $otherTable,
        $foreignCol = 'id',
        $nullable = false,
        $foreignKey = true
    ) {
        $localCol = $otherTable . '_id';
        $colSpec = $t->bigInteger($localCol)->unsigned();

        if ($nullable) {
            $colSpec->nullable();
        }

        if ($foreignKey) {
            $t->foreign($localCol)
                ->references($foreignCol)
                ->on($otherTable)
                ->onUpdate('cascade')
                ->onDelete('cascade');
        }
    }

    /**
     * Because sometimes a migration fails.
     */
    protected function forceCreate($table, $cb)
    {
        if (Schema::hasTable($table))
            Schema::drop($table);

        Schema::create($table, $cb);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->forceCreate('type', function (Blueprint $t) {
            $t->bigIncrements('id');

            $t->string('name');
            $t->string('kind');

            $t->timestamps();
            $t->softDeletes();
        });

        $this->forceCreate('contact', function (Blueprint $t) {
            $t->bigIncrements('id');

            $t->text('fields');
            // will be automagically json_{en,de}coded if the model declares
            // protected $casts = [
            //     'fields' => 'array',
            // ];
            $t->decimal('latitude', 12, 8)->nullable();
            $t->decimal('longitude', 12, 8)->nullable();
            $t->text('image_url')->nullable();

            $t->timestamps();
            $t->softDeletes();
        });

        $this->forceCreate('user', function (Blueprint $t) {
            $t->bigIncrements('id');

            $this->relate($t, 'type', 'id', false, false);
            $t->string('name');
            $t->string('email')->unique();
            $t->string('password');

            $t->rememberToken();
            $t->timestamps();
            $t->softDeletes();
        });

        $this->forceCreate('request', function (Blueprint $t) {
            $t->bigIncrements('id');

            $t->string('name');
            $t->text('notes')->nullable();

            $t->timestamps();
            $t->softDeletes();
        });

        $this->forceCreate('resource', function (Blueprint $t) {
            $t->bigIncrements('id');

            $t->string('name');

            $t->timestamps();
            $t->softDeletes();
        });

        $this->forceCreate('status', function (Blueprint $t) {
            $t->bigIncrements('id');

            $this->relate($t, 'type', 'id', false, false);

            $t->string('name');
            $t->string('path');

            $t->timestamps();
            $t->softDeletes();
        });

        $this->forceCreate('password_resets', function (Blueprint $t) {
            $t->string('email')->index();
            $t->string('token')->index();
            $t->timestamps();
        });

        foreach ([
            'contact_user' => [
                'contact',
                'user',
            ],
            'contact_resource' => [
                'contact',
                'resource',
            ],
            'contact_request' => [
                'contact',
                'request',
            ],
            'request_resource' => [
                'request',
                'resource',
            ],
            'request_status' => [
                'request',
                'status',
            ],
            'request_type' => [
                'request',
                'type',
            ],
            'request_user' => [
                'request',
                'user',
            ],
        ] as $joinTable => $tables) {
            $this->forceCreate($joinTable, function (Blueprint $t) use ($tables) {
                $this->relate($t, $tables[0]);
                $this->relate($t, $tables[1]);
                // $t->boolean('current'); //denormalize across time!?
                $t->timestamps();
                $t->softDeletes();
            });
        }

        // Schema::table('contact', function ($t) {

        //     foreach ([
        //         'contact_user',
        //         'contact_request',
        //         'contact_resource',
        //     ] as $jt) {
        //     $t->foreign('id')
        //         ->references('contact_id')
        //         ->on($jt)
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        //     }
        // });



        // $table->foreign('user_id')
        //       ->references('id')->on('user')
        //       ->onDelete('cascade');

        // the following two are the same
        // $table->dropForeign('posts_user_id_foreign');
        // $table->dropForeign(['user_id']);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ([
            'contact',
            'user',
            'request',
            'password_resets',
            'resource',
            'type',
            'contact_user',
            'contact_request',
            'contact_resource',
            'request_resource',
            'request_status',
            'request_type',
            'request_user',
            'type_user',
        ] as $t) {
            Schema::drop($t);
        }
    }
}
