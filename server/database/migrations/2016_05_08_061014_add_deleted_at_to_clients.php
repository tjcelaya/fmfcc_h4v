<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAtToClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oauth_scopes', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('oauth_clients', function (Blueprint $t) {
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oauth_scopes', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('oauth_clients', function (Blueprint $t) {
            $t->softDeletes();
        });
    }
}
