<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(App\Model\User::class)->make();

        factory(App\Model\User::class)
            ->create()
            ->each(function($u) {
                $reqs = rand(0,5);
                $r = factory(App\Model\Request::class, $reqs)->create();
                $u->requests()->saveMany($r);
            });

        // eval(\Psy\sh());
        // dd();
    }
}
