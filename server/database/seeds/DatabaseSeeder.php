<?php

use App\Model\Contact;
use App\Model\OAuth\Client as OAuthClient;
use App\Model\OAuth\Scope as OAuthScope;
use App\Model\Permission;
use App\Model\Request as RequestModel;
use App\Model\Resource;
use App\Model\Role;
use App\Model\Status;
use App\Model\Status\Type as StatusType;
use App\Model\Type;
use App\Model\User;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach ([
                     'admin',
                     'rep',
                 ] as $id => $role) {
            $roles[$role] = new Role([
                'id' => $id + 1,
                'name' => $role,
                'display_name' => titleize($role),
            ]);

            $roles[$role]->save();
        }

        foreach ([
                     'request_associate',
                     'request_associateable',
                 ] as $id => $permission) {
            $permissions[$permission] = new Permission([
                'id' => $id + 1,
                'name' => $permission,
                'display_name' => titleize($permission),
            ]);

            $permissions[$permission]->save();
        }

        $roles['admin']->permissions()->attach($permissions['request_associate']);
        $roles['rep']->permissions()->attach($permissions['request_associateable']);

        (new Type([
            'id' => Type::CONTACT,
            'name' => 'Contact',
        ]))->save();

        (new Type([
            'id' => Type::REQUEST,
            'name' => 'Request',
        ]))->save();

        (new Type([
            'id' => Type::STATUS,
            'name' => 'Status',
        ]))->save();

        (new Type([
            'id' => Type::USER,
            'name' => 'User',
        ]))->save();

        $completeStatus = new Status([
            'id' => 1,
            'name' => 'Done',
            'type_id' => StatusType::REQUEST,
            'path' => '1',
        ]);

        $completeStatus->save();
        $statuses = factory(Status::class, 10)->create();

        $resources = factory(Resource::class, rand(5, 10))
            ->create();

        $users = factory(User::class, rand(5, 10))
            ->create()
            ->each(function ($u) {
                $someContacts = enlist(
                    factory(Contact::class, rand(0, 2))
                        ->create()
                );
                $u->contacts()->saveMany($someContacts);
            });

        $adminUser = $users->first();
        $adminUser->email = 'admin@admin.admin';
        $adminUser->password = Hash::make('admin');
        $adminUser->save();
        $adminUser->roles()->save($roles['admin']);


        $roles['rep']->users()->saveMany($users->slice(1));

        factory(RequestModel::class, 20)
            ->create()
            ->each(function ($r) use ($users, $completeStatus, $statuses) {
                // $r->users()->saveMany(array_sample($users, rand(1,3)));

                // attach pre-created things
                foreach ([
                             'statuses' => $statuses,
                             'users' => $users,
                         ] as $relName => $things) {

                    foreach (array_sample($things, rand(0, 4)) as $thing) {
                        $assignTime = Carbon\Carbon::now()->subMinutes(rand(4, 100000));

                        // attach a thing, or attach it as "removed"
                        if (coinflip()) {
                            $r->{$relName}()->attach($thing, timestamps($assignTime), true);
                        } else {
                            $unassignTime = $assignTime->addMinutes(rand(4, 100000));
                            $r->{$relName}()->attach($thing, timestamps($assignTime, $assignTime, $unassignTime), true);
                        }
                    }
                }

                $r->contacts()->saveMany(enlist(factory(Contact::class, rand(0, 2))->create()));

                if (coinflip()) {
                    $r->statuses()->save($completeStatus);
                }

                if (coinflip()) {
                    $r->delete();
                }
            });

        OAuthClient::create([
            'id' => 1,
            'secret' => env('APP_OAUTH_ADMIN_CLIENT_SECRET', 'root_secret'),
            'name' => env('APP_OAUTH_ADMIN_CLIENT_NAME', 'root'),
        ]);

        foreach (get_things() as $thing) {
            OAuthScope::create([
                'id' => $thing,
                'description' => $thing,
            ]);
        }

        // $r = new HttpRequest;
        // $r->setMethod('POST');
        // $r->replace([
        //     'grant_type' => 'client_credentials',
        //     'client_id' => '1',
        //     'client_secret' => '123',
        //     'scopes' => implode(get_things()),
        // ]);
        // Authorizer::setRequest($r);

        // $t = Authorizer::issueAccessToken();

        // system('echo "'.json_encode($t, JSON_PRETTY_PRINT).'"');

        // override globals for auth server to intercept

        // app('Dingo\Api\Dispatcher')->post('/oauth/access_token');

        // $this->call(RequestTableSeeder::class);
        // $this->call(UserTableSeeder::class);

        // Model::reguard();
    }
}
