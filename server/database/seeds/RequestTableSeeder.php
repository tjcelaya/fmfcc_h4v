<?php

use Illuminate\Database\Seeder;

class RequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Model\Request::class, 1)
            ->create()
            ->each(function($r) {
                $users = rand(2,5);

                $us = factory(App\Model\User::class, $users)->create();
                $r->users()->saveMany($us);
            });
    }
}
