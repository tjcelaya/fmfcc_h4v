clear \
  && php -v \
  && toilet -F gay "DB" \
  && mysql -uroot -h127.0.0.1 -psecret -e'drop database fmfcc_hv; create database fmfcc_hv;' \
  && toilet -F gay "Migrate" \
  && ./artisan migrate:refresh -vvv \
  && ./artisan migrate:status -vvv \
  && toilet -F gay "Seed" \
  && ./artisan db:seed -vvv \
  && toilet -F gay "Test" \
  && vendor/bin/phpunit \
  && toilet -F gay "Root Token" \
  && curl -X POST http://localhost:8000/oauth/access_token \
   -F 'grant_type=client_credentials' \
   -F 'client_id=1' \
   -F 'client_secret=root_secret' \
   -F 'scope=contact,request,resource,status,type,user' \
  && echo

# we used to have composer between DB and Migrate but
# composer triggered the fswatch too, this is NOT GOOD
  #&& toilet -F gay "Composer" \
  #&& composer dumpautoload \

