# Server for FMFCC-H4V Project

## Install!
```
composer install
./artisan serve     # goto http://localhost:8000
```

## Test!

run `test.sh`
contents discussed here.

```
clear \
  && toilet "DB" \                # drop and recreate the database
  && mysql -uroot -h192.168.99.100 -psecret -e'drop database fmfcc_h4v; create database fmfcc_h4v;' \
  && toilet "Composer" \
  && composer dumpautoload \      # fix php autoloading
  && toilet "Migrate" \
  && ./artisan migrate:refresh \  # prepare the test database
  && ./artisan migrate:status \
  && toilet "Seed" \
  && ./artisan db:seed \          # seed the test database
  && toilet "Test" \
  && phpunit                      # run the test suite
```

You should get something that looks like ![this](test_fmfcc_h4v.png)

## Generating Models

The `ignasbernotas/laravel-model-generator` package was used to autogenerate Laravel models from the database schema. Nothing special was done to create the schema other than add some helpers to the `database/migrations/2016_03_21_020428_create_models.php` file to make iterating faster.

[gotoit](database/migrations/2016_03_21_020428_create_models.php)
