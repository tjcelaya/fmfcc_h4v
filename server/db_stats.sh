#set -x
source .env

M="mysql -u$DB_USERNAME -p$DB_PASSWORD -h$DB_HOST $DB_DATABASE"
QUERY_TABLES="select table_name from information_schema.tables where table_schema = '$DB_DATABASE';"
TABLE_AI=$($M -N -e"select auto_increment from information_schema.tables where table_schema = '$DB_DATABASE'")

# -N supresses column headers, -e executes a stmt
$M -N -e"$QUERY_TABLES"|\
while read tbl; do
  $M -e"select count(*) as $tbl from $tbl"
  #$M -e"select auto_increment from information_schema.tables where table_schema = '$DB_DATABASE'"
done|\
  sed 'N;s/\n/ /'|\
  awk '{printf("%20s %s\n",$1,$2)}'

