<?php

return [
    [
        'class' => \App\Model\Request::class,
        'alias' => 'Request',
        'aliased_properties' => [
        ],
        'hide_properties' => [
        ],
        'id_properties' => [
            'id',
        ],
        'urls' => [
            'self' => 'request.show', //named route
        ],
    ],
];
